<?php namespace Finnito\ArcdocsTheme\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
// use Anomaly\Streams\Platform\Http\Controller\AdminController;
// use Finnito\MembersModule\User\Form\MembersFormBuilder;
// use Illuminate\Contracts\Auth\Guard;

class ThemeController extends PublicController {
    public function disable() {
        abort(404);
    }
}
